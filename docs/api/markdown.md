---
title: MarkDown Api
sidebar: auto
sidebarDepth: 2
---
## MarkDown是什么
**Markdown**是一种轻量级标记语言，创始人是约翰·格鲁伯（John Gruber）。它允许人们 “使用易读易写的纯文本格式编写文档，然后转换成有效的 HTML 文档。

## MarkDown的优势

- **语法简单**：Markdown 的语法是一些简单而且常用的标记符号，任何人只要花几分钟就能学会；
- **兼容性强**：Markdown 是兼容性非常强的纯文本内容，可以使用任何编辑器打开，格式都不会乱；
- **导出方便**：Markdown 可以导出 PDF、Word、HTML、Epub、LaTeX 等文件格式；
- **专注内容**：使用 Markdown 写作再也不用纠结排版，其简洁优雅的格式会让你沉浸到写作的乐趣之中；
- **团队协作**：Github、Gitlab、Gitbook 都支持 Markdown，团队协作再也不是问题了；

## MarkDown Api
